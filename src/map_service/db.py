from typing import List

from sqlalchemy import (
    Column,
    Integer,
    String,
    create_engine
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from . import SearchItem, Config

Base = declarative_base()


class Service(Base):

    __tablename__ = 'services'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    location = Column(String, nullable=False)
    website = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    mail = Column(String, nullable=False)


engine = create_engine(Config.DB_URI)

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()


def write_object(
        item: SearchItem,
) -> None:
    new_item = Service(
        name=item.name,
        location=item.location,
        phone=item.phone,
        website=item.website,
        mail=item.mail
    )
    session.add(new_item)
    session.commit()


def write_objects(
        items: List[SearchItem],
) -> None:
    for item in items:
        write_object(item=item)
