from __future__ import annotations

import asyncio
import dataclasses
import json
import random
import re
import time

from playwright.async_api import async_playwright
from playwright.async_api import TimeoutError
from playwright.async_api import Page, BrowserContext
from bs4 import BeautifulSoup, Tag


class Config:

    """
    Represents a global configuration object
    """

    '''Pages, which will be parsed simultaneously, after search query'''
    MAX_SIMULTANEOUSLY_PAGES: int = 8

    '''Timeout, which applies when parsing email from website'''
    EMAIL_PARSING_TIMEOUT_MS = 5000

    '''Timeout, which applies when rechecking count of parsing items'''
    RECHECK_ITEM_COUNT_TIMEOUT_MS = 7000

    RECHECK_ITEM_COUNT_OFFSET_MS = 1000

    DB_URI = 'sqlite:///test_database.db'

    DEBUG = True

    _DEFAULT_FILE_NAME = 'default.config'

    @classmethod
    def read_string(cls, config_string: str) -> None:
        data = json.loads(config_string)
        cls.MAX_SIMULTANEOUSLY_PAGES = data['MAX_SIMULTANEOUSLY_PAGES']
        cls.EMAIL_PARSING_TIMEOUT_MS = data['EMAIL_PARSING_TIMEOUT_MS']
        cls.RECHECK_ITEM_COUNT_TIMEOUT_MS = data['RECHECK_ITEM_COUNT_TIMEOUT_MS']
        cls.DB_URI = data['DB_URI']
        cls.DEBUG = data['DEBUG']

    @classmethod
    def dump_string(cls) -> str:
        return json.dumps({
            'MAX_SIMULTANEOUSLY_PAGES': cls.MAX_SIMULTANEOUSLY_PAGES,
            'EMAIL_PARSING_TIMEOUT_MS': cls.EMAIL_PARSING_TIMEOUT_MS,
            'RECHECK_ITEM_COUNT_TIMEOUT_MS': cls.RECHECK_ITEM_COUNT_TIMEOUT_MS,
            'DB_URI': cls.DB_URI,
            'DEBUG': cls.DEBUG,
        }, indent=4)

    @classmethod
    def create_default_config_file(cls) -> str:
        with open(cls._DEFAULT_FILE_NAME, 'w') as file:
            dumped = cls.dump_string()
            file.write(dumped)


class LOCATORS:

    """
    Locators for different elements
    """

    ITEM_INFO_ELEMENT = 'div[role="main"] > div[role="region"][style=""] > div'
    ITEM_NAME = 'div[role="main"] h1'
    ITEM_INFO_LOCATION = 'img[src*="place_gm_blue_24dp.png"]'
    ITEM_INFO_WEBSITE = 'img[src*="public_gm_blue_24dp.png"]'
    ITEM_INFO_CODE = 'img[src*="ic_plus_code.png"]'
    ITEM_INFO_PHONE = 'img[src*="phone_gm_blue_24dp.png"]'

    SEARCH_TABLE = 'div[role="feed"]'

    RAW_ITEM = 'div[role="article"] > a'


@dataclasses.dataclass
class SearchDataType:
    name: str
    location: str
    phone: str
    website: str
    mail: str


@dataclasses.dataclass
class SearchItem(SearchDataType):

    """
    Represents search item, which will be build after search query
    """

    def __post_init__(self) -> None:

        if Config.DEBUG:
            print(f"""

                Page data:
                {self.name=}
                {self.location=}
                {self.phone=}
                {self.website=}
                {self.mail=}

            """)

    @staticmethod
    def is_location(tag: Tag) -> bool:
        return bool(tag.select_one(LOCATORS.ITEM_INFO_LOCATION))

    @staticmethod
    def is_phone(tag: Tag) -> bool:
        return bool(tag.select_one(LOCATORS.ITEM_INFO_PHONE))

    @staticmethod
    def is_website(tag: Tag) -> bool:
        return bool(tag.select_one(LOCATORS.ITEM_INFO_WEBSITE))

    @staticmethod
    def extract_string(tag: Tag) -> str:
        text = ''.join(list(tag.stripped_strings))
        return text.strip()

    @classmethod
    def extract_page(cls, soup: BeautifulSoup) -> dict:
        """
        Extracts a general and additional information about service
        from soup object
        :param soup:
        :return:
        """

        init_data = {
            'mail': '',
            'phone': '',
            'website': '',
            'location': '',
        }
        checkers = {
            'location': cls.is_location,
            'phone': cls.is_phone,
            'website': cls.is_website,
        }

        for tag in soup.select(LOCATORS.ITEM_INFO_ELEMENT):
            for key, checker in checkers.items():

                found = checker(tag)

                if found and key == 'website':
                    init_data[key] = tag.find('a').attrs['href']

                elif found:
                    init_data[key] = cls.extract_string(tag)

        return init_data

    @classmethod
    async def create_from_url(
            cls,
            context: BrowserContext,
            url: str,
            semaphore: asyncio.Semaphore,
            tries: int = 3,
    ) -> SearchItem:
        """
        Create a SearchItem object from url.
        Using semaphore for limit simultaneous pages
        :param tries:
        :param context:
        :param url:
        :param semaphore:
        :return:
        """

        if tries <= 0:
            return

        async with semaphore:

            page = await context.new_page()

            try:
                await page.goto(url)

            except TimeoutError:
                await page.close()
                return await cls.create_from_url(
                    context=context,
                    url=url,
                    semaphore=semaphore,
                    tries=tries - 1
                )

            soup = BeautifulSoup(await page.content(), 'html.parser')
            name = cls.extract_string(soup.select_one(LOCATORS.ITEM_NAME))
            init_data = cls.extract_page(soup=soup)

            if init_data['website']:
                init_data['mail'] = await cls.extract_email(
                    url=init_data['website'],
                    page=page
                )

            if not page.is_closed():
                await page.close()

            return cls(name=name, **init_data)

    @staticmethod
    async def extract_email(url: str, page: Page) -> str:
        """
        Using for extract email from service, which have a
        website.
        This function not guarantee a mail from website
        :param url:
        :param page:
        :return:
        """

        result = ''

        def is_email_tag(tag: Tag) -> bool:
            return re.search(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", tag.text)

        try:
            await page.goto(url, timeout=Config.EMAIL_PARSING_TIMEOUT_MS)
            soup = BeautifulSoup(await page.content(), 'html.parser')
            email_tag = soup.find(is_email_tag)
            email_match = re.search(
                r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$",
                email_tag.text
            )
            result = email_match.group()

        finally:
            await page.close()
            return result

    @staticmethod
    async def _item_urls_list(page: Page):
        """
        Help function, which gives a list of services
        extracted from Google Maps
        :param page:
        :return:
        """

        soup = BeautifulSoup(await page.content(), 'html.parser')
        return [
            element.attrs['href']
            for element in soup.select(LOCATORS.RAW_ITEM)
        ]

    def __repr__(self):
        return f'< SearchItem `{self.name=}` `{self.website=}` >'


class SearchResult:

    def __init__(self, search: Search):
        self.search = search

    async def parse(self):
        """
        Parse a search results for SearchItem objects,
        which using for storing some information about service
        :return:
        """

        start = time.perf_counter()
        items = await SearchItem._item_urls_list(page=self.search.search_page)
        semaphore = asyncio.Semaphore(Config.MAX_SIMULTANEOUSLY_PAGES)
        tasks = []

        for url in items:
            future = asyncio.ensure_future(
                SearchItem.create_from_url(self.search.context, url, semaphore)
            )
            tasks.append(future)

        result = await asyncio.gather(*tasks)
        end = time.perf_counter()

        if Config.DEBUG:
            print(f'Evaluated parsing with time - {end-start} seconds')

        return list(filter(bool, result))


class create_search:

    """
    Search class context manager
    """

    def __init__(
            self,
            location: str,
            query: str,
            item_count: int = 100,
            **browser_options
    ):
        self.location = location
        self.query = query
        self.item_count = item_count
        self.browser_options = browser_options
        self.search: Search = None

    async def __aenter__(self) -> SearchResult:
        self.search = await Search.create(
            location=self.location,
            query=self.query,
            item_count=self.item_count,
            **self.browser_options
        )
        await self.search.start_query()
        return SearchResult(search=self.search)

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.search.context.browser.close()


class Search:

    """
    Represents a search, in which user types a query
    """

    def __init__(
            self,
            location: str,
            query: str,
            context: BrowserContext,
            item_count: int = 100
    ):
        self.location = location
        self.query = query
        self.context = context
        self.item_count = item_count
        self.search_page: Page = None

    async def get_raw_items_count(self) -> int:
        """
        Returns count of items, which currently displaying in a page
        :return:
        """
        return await self.search_page.locator(LOCATORS.RAW_ITEM).count()

    async def start_query(self) -> None:
        """
        Start a search query, and it will be done
        after `item_count` will equal to
        services count in a page
        :return:
        """

        start_time = time.perf_counter()
        self.search_page = await self.context.new_page()
        await self.search_page.goto(self.query_string)
        await self.search_page.focus(LOCATORS.SEARCH_TABLE)

        while True:

            if await self.get_raw_items_count() >= self.item_count:
                break

            randomized = random.randint(
                Config.RECHECK_ITEM_COUNT_TIMEOUT_MS - Config.RECHECK_ITEM_COUNT_OFFSET_MS,
                Config.RECHECK_ITEM_COUNT_TIMEOUT_MS + Config.RECHECK_ITEM_COUNT_OFFSET_MS
            )

            actions_list = [
                lambda: self.search_page.keyboard.down('PageDown'),
                lambda: self.search_page.keyboard.down('End'),
                lambda: self.search_page.keyboard.down('ArrowDown'),
                lambda: self.search_page.mouse.wheel(0, 4000)
            ]

            await asyncio.sleep(randomized / 1000)
            await random.choice(actions_list)()

            await self.search_page.keyboard.down('PageDown')
            await self.context.clear_cookies()

        end_time = time.perf_counter()

        if Config.DEBUG:
            print(f'Evaluated searching with time - {end_time-start_time} seconds')

    @property
    def query_string(self) -> str:
        """
        Making a query string from `location` and `query` attributes
        :return:
        """
        location = self.location.replace(' ', '+')
        query = self.query.replace(' ', '+')
        return f"https://www.google.com/maps/search/{query}+in+{location}"

    @classmethod
    async def create(
            cls,
            location: str,
            query: str,
            item_count: int = 100,
            **browser_options,
    ) -> Search:
        """
        Creating a Search object with async function
        :param location:
        :param query:
        :param item_count:
        :param browser_options:
        :return:
        """
        browser_options.setdefault('headless', False)
        playwright = await async_playwright().start()
        browser = await playwright.chromium.launch(**browser_options)
        context = await browser.new_context()
        return cls(
            location=location,
            query=query,
            item_count=item_count,
            context=context
        )
