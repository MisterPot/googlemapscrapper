import asyncio

from map_service import create_search, Config
from map_service.db import write_objects


async def main() -> None:

    with open('default.config') as config:
        Config.read_string(config_string=config.read())

    async with create_search(
        location='Paris',
        query='cafe',
        item_count=30,
        headless=False,
    ) as search_result:
        parsed_items = await search_result.parse()
        write_objects(items=parsed_items)
        print(parsed_items)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
